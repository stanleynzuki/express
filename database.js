var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser'); // Import body-parser module


var app = express()

app.set('view engine', 'pug');
app.set('views', './views');

// Use body-parser middleware to parse incoming data
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/my_db');

//Create model Person
var personSchema = mongoose.Schema({
    name: String,
    age: Number,
    nationality: String
});
var Person = mongoose.model("Person", personSchema);

app.get('/person', function (req, res) {
    res.render('person');
});

app.post('/person', async function (req, res) {
    var personInfo = req.body;

    // Validate
    if (!personInfo.name || !personInfo.age || !personInfo.nationality) {
        res.render('show_message', {
            message: "Sorry you provided wrong info", type: 'error'
        });
    } else {
        var newPerson = new Person({
            name: personInfo.name,
            age: personInfo.age,
            nationality: personInfo.nationality
        });

        try {
            await newPerson.save(); // Use await with the save method
            res.render('show_message', {
                message: 'New person added!', type: 'success', person: personInfo
            });
        } catch (err) {
            res.render('show_message', {
                message: 'Database error!', type: 'error'
            });
        }
    }
});

//Fetch all

//Display all on view
app.get('/people', function (req, res) {
    var persons = null;
    async function getPersons() {
        persons = await Person.find();
        res.render('people', {
            people: persons
        })



    }
    getPersons()

})

//Display all on view
app.get('/person/:id', function (req, res) {
    var person = null;
    async function getPerson() {
        person = await Person.findById(req.params.id);
        res.render('details', {
            person: person
        })
    }
    getPerson()

})



//Fetch by name and age
async function getByNameandAge() {
    const persons = await Person.find({ name: 'Dr', age: 12 });
    console.log(persons);
}
//getByNameandAge()

//find one
async function getOne() {
    const person = await Person.findOne();
    console.log(person);
}

//getOne()

async function getById() {

    const person = await Person.findById('64e381607409090ab34a6310');
    console.log(person);
}

//getById();


//Update all
async function updateOne(){
    var response = await Person.findOneAndUpdate({name:'Dr'}, {age:999})
    console.log(response);
    
}
//updateOne()

//Display person by id
app.get('/update/:id', function (req, res) {
    var person = null;
    async function getPerson() {
        person = await Person.findById(req.params.id);
        res.render('update', {
            person: person
        })
    }
    getPerson()

})

//Update person by id
app.post('/update/:id', function (req, res) {
    console.log(req.body.age)
    async function updateById() {
        var person = await Person.findByIdAndUpdate(req.params.id, 
            {name:req.body.name,age:req.body.age,
            nationality:req.body.nationality});
        
        res.redirect("/people")
    }
    updateById()

})


//delete by id
app.get('/delete/:id', function (req, res) {
    async function deleteById() {
        var person = await Person.findByIdAndDelete(req.params.id);
        res.redirect("/people")
    }
    deleteById()

})











app.listen(3000)