var express = require('express');
var app = express()

app.get("/hello", function(req,res){
    res.send("You get hello!")
})

app.post("/hello", function(req,res){
    res.send("You called post!")
})

app.all("/test", function(req,res){
    res.send("Am all")
})

app.listen(3000);