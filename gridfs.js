var express = require('express');
var mongodb = require('mongodb');
var bodyParser = require('body-parser');
var fs = require('fs')
var multer = require('multer')
const { MongoClient, ServerApiVersion } = require("mongodb");
// Replace the placeholder with your Atlas connection string



var app = express()

app.set('view engine', 'pug');
app.set('views', './views');
app.use(express.static('public'));
app.use('/static', express.static('public'));
app.use('/uploads', express.static('uploads'));



// Use body-parser middleware to parse incoming data
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


const uri = "mongodb://localhost/";
// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri,  {
        serverApi: {
            version: ServerApiVersion.v1,
            strict: true,
            deprecationErrors: true,
        }
    }
); // Import body-parser module

const db = client.db("my_db");
const bucket = new mongodb.GridFSBucket(db, { bucketName: 'myCustomBucket' });
//storage
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

app.get("/", function (req, res) {
    res.render("upload")
})

app.post("/upload", upload.single('file'), (req,res) => {
    if(!req.file){
        return res.status(400).send('No file uploaded1');
    }else{
        bucket.openUploadStream('nowf', {
            chunkSizeBytes: 1048576,
            metadata: { field: 'myField', value: 'myValue' }
          }).end(req.file.buffer);
          res.send('File uploaded successfully!')



     const cursor = bucket.find({});
   

     
    }

});

//Download file
app.get("/open_file", function(req,res){
    bucket.openDownloadStreamByName('nowf').
     pipe(fs.createWriteStream('./now.jpeg'));
        res.send('File downloaded successfully!')
})

app.get('/preview', (req, res) => {
    const fileName = '65099306a07eb1a8cb3e2c62'; // Retrieve the file ID from the query parameter
    const downloadStream = bucket.openDownloadStream(new mongodb.ObjectId(fileName));
  
    // Set appropriate content type based on file type (e.g., image/jpeg, application/pdf)
    res.setHeader('Content-Type', 'image/jpeg');
  
    // Pipe the file data to the response
    downloadStream.pipe(res);
  });

  
  app.get('/preview1', (req, res) => {
  const fileId = '65099306a07eb1a8cb3e2c62'; // Retrieve the file ID from the query parameter
  const downloadStream = bucket.openDownloadStream(new mongodb.ObjectId(fileId));

  // Set appropriate content type based on file type (e.g., image/jpeg, application/pdf)
  res.setHeader('Content-Type', 'image/jpeg'); // Adjust content type as needed

  // Create a buffer to store the file data
  const fileData = [];

  console.log('Starting downloadStream');
  
  downloadStream.on('data', (chunk) => {
    console.log('Received data chunk:', chunk.length, 'bytes');
    fileData.push(chunk);
  });

  downloadStream.on('end', () => {
    console.log('downloadStream ended');
    console.log('Total data collected:', fileData.length, 'bytes');
    
    // Now that all data is collected, render the Pug template with the data
    res.render('preview', { fileData: Buffer.concat(fileData) });
  });

  downloadStream.on('error', (error) => {
    console.error('Error while downloading file:', error);
    res.status(500).send('Error while downloading file');
  });
});

  


  

app.listen(3000)