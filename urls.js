var express = require('express')

var app = express()

app.get("/:id", function(req,res){
    res.send("The id specified is "+ req.params.id)
})

app.get("/things/:name/:id", function(req,res){
    res.send("id: "+req.params.id+", name: "+req.params.name)
})

app.get("/things/:id([0-9]{5})", function(req,res){
    res.send("id: "+req.params.id)
})

app.get("*", function(req,res){
    res.send("Sorry, the url is invalid")
})
app.listen(3000)