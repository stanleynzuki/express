var express = require("express")
var bodyParser = require('body-parser')
var multer = require('multer')
var upload = multer();

var app = express();

app.set('view engine', 'pug');
app.set('views','./views');
app.use(express.static('public'));
app.use('/static', express.static('public'));

//for parsing application/json
app.use(bodyParser.json());

//for passing application/xwww-
app.use(bodyParser.urlencoded({extended:true}))

//for passing multipart/form-data
app.use(upload.array())




app.get('/first_template', function(req, res){
    res.render('first_view');
 });

 //dynamic view
 app.get('/dynamic_view', function(req,res){
    res.render('dynamic',{
        name:'Stanley Nzuki',
        url: 'https://www.stansoftcorporation.com'
    })

 });

 //Pass user object to conditional pug
 app.get('/conditional', function(req,res){
    res.render('conditionals', {
        user:{
            name:'Stanley Nzuki'
        }
    })
 })

 //Include and components
 app.get("/components", function(req,res){
    res.render('content')
 });


 //Static files
 app.get('/static_test', function(req,res){
    res.render('static')
 })

 //display form
 app.get('/form', function(req,res){
    res.render('form')
 });

 //submit form
 app.post('/form', function(req, res){
    console.log(req.body);
    res.send("Recieved your request")
 })

 app.listen(3000)