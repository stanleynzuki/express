var express = require('express')
var router = express.Router();

router.get("/", function(req,res){
    res.send("Welcome home")
})

router.post("/", function(req,res){
    res.send("Go home!")
})

module.exports = router;