var express = require('express');
var cookieParser = require('cookie-parser');

var app = express();
app.use(cookieParser());


//set cookie
app.get("/", function(req, res) {
    res.cookie('name', 'express', {expire: 360000 + Date.now()}).send('Cookie set');
});

app.get("/print", function(req, res) {
    console.log(req.cookies); // Access cookies from the request object
    res.send('Cookies printed in the console');
});

//Clear cookie
app.get("/clear_cookie", function(req,res){
    res.clearCookie("name");
    res.send("Cookie name cleared!");
})

app.listen(3000, function() {
    console.log("Server is running on port 3000");
});
