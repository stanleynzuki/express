var express  = require('express')
const {Pool} = require('pg')

var app = express()
const pool = new Pool({
    user: 'postgres',
  host: 'localhost',
  database: '',
  password: '',
  port: 5432, // default PostgreSQL port
})

//Insert
app.post("/insert", async(req,res) =>{
    const {email,password} = {email:'dragon@fire.com', password:'welcomefruits'}
    try{
        const query = "INSERT INTO users (email,password) VALUES ($1,$2)";
        await pool.query(query,[email,password])
        res.send("Data inserted!")

    }catch(e){
        console.log("Error inserting data: "+e);
        res.status(500).send("Error inserting data");
    
    }
})

//select data
app.get("/select", async(req,res)=>{
    try{
        const result = await pool.query("SELECT * FROM users");
        res.json(result.rows)
    }catch(e){
        console.log("An error occured: "+e);
    }


})


app.listen(3000)