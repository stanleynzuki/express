var express = require('express');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var bodyParser  = require('body-parser');
var multer = require('multer');
var upload = multer()

var app = express();
app.use(express.json());

//for passing application/xwww-
app.use(bodyParser.urlencoded({extended:true}))

//for passing multipart/form-data
app.use(upload.array())

const users = []

function createUser(id, username, password) {
    return { id, username, password };
  }

app.post("/register", (req,res)=>{
    var id = 1;
    if(users.length>0){
        id = users[users.length-1].id+1
    }
    console.log(id)
    users.push(
        createUser(
            id,
            req.body.username,
            bcrypt.hashSync(req.body.password,10)
        )
    )
    res.status(201).send("User registered successfully!")
})

app.post("/login", (req,res)=>{
    const {username,password} = req.body;
    const user = users.find(user =>user.username ==username);

    if(!user || !bcrypt.compareSync(password, user.password)){
        res.status(500).send("Invalid credentials!");
    }else{
        const token = jwt.sign({id:user.id,user:username}, 'secret_key');
        res.json({token});
    }
})

app.get("/profile", (req,res)=>{
    const token = req.headers.authorization;
    if(!token){
        res.status(500).send("Unauthorized!");
    }else{
        try{
            const decoded = jwt.verify(token, 'secret_key')
            res.json({message:'Access granted to protected route', user:decoded})
        }catch(e){
            res.status(401).json({message:'Invalid token!'})
        }
    }
})
app.listen(3000)