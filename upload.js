var express = require('express');
var bodyParser = require('body-parser')
var multer = require('multer')
var path = require('path')

var app = express()

app.set('view engine', 'pug');
app.set('views', './views');
app.use(express.static('public'));
app.use('/static', express.static('public'));
app.use('/uploads', express.static('uploads'));



//storage
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/')
    },
    filename: (req, file, cb) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
        cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname));
    }
})

const upload = multer({storage})

app.get("/", function (req, res) {
    res.render("upload")
})

app.post("/upload", upload.single('file'), (req,res) => {
    if(!req.file){
        return res.status(400).send('No file uploaded1');
    }else{
        res.send('File uploaded successfully!')
    }

});
app.listen(3000)